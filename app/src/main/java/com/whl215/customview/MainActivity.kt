package com.whl215.customview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.whl215.customview.databinding.ActivityMainBinding
import com.whl215.customview.flipboard.FlipboardActivity
import com.whl215.customview.jk.JiKeActivity
import com.whl215.customview.mint.MintHealthActivity

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var mBinding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mBinding.btnGoJiKe.setOnClickListener(this)
        mBinding.btnGoMint.setOnClickListener(this)
        mBinding.btnGoFlipboard.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        if (view == mBinding.btnGoJiKe) {
            startActivity(Intent(this, JiKeActivity::class.java))
        } else if (view == mBinding.btnGoMint) {
            startActivity(Intent(this, MintHealthActivity::class.java))
        } else if (view == mBinding.btnGoFlipboard) {
            startActivity(Intent(this, FlipboardActivity::class.java))
        }
    }
}