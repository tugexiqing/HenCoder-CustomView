package com.whl215.customview.flipboard

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.whl215.customview.databinding.ActivityFlipboardBinding

class FlipboardActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityFlipboardBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityFlipboardBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

    }
}