package com.whl215.customview.mint

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.whl215.customview.databinding.ActivityMintHealthBinding

/**

 */
class MintHealthActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityMintHealthBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMintHealthBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mBinding.tvRuler.onValueChangeListener = object :RulerView.OnValueChangeListener{
            override fun onChange(value: Float) {
                mBinding.tvValue.text = value.toString()
            }
        }
    }
}