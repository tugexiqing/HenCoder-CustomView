package com.whl215.customview.jk

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.animation.OvershootInterpolator
import androidx.appcompat.widget.LinearLayoutCompat

class TouchAnimLayout : LinearLayoutCompat {
    private val touchAnimList = mutableListOf<ITouchAnimInterface>()

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    init {
        isClickable = true
        isFocusable = true
        gravity = Gravity.CENTER_VERTICAL
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        for (i in 0..childCount) {
            val view = getChildAt(i)
            if (view is ITouchAnimInterface) {
                touchAnimList.add(view)
                break
            }
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startDownAnim()
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                startUpAnim()
            }
        }
        return super.onTouchEvent(event)
    }

    private fun startDownAnim() {
        Log.d("aaa","size ${touchAnimList.size}")
        for (anim in touchAnimList) {
            anim.startAnim(true)
        }
    }

    private fun startUpAnim() {
        for (anim in touchAnimList) {
            anim.startAnim(false)
        }
    }
}