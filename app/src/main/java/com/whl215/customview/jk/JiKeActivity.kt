package com.whl215.customview.jk

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.whl215.customview.databinding.ActivityJiKeBinding

/**
业务分析：
1.点赞  点击动画，点赞成功光圈，三个竖条，数字增加滚动，无数字不展示 图左数字右
2.回复,分享，其他  点击动画，数字不需要动态增加， 无其他效果 图左数字右
3.评论点赞 点击动画 无光圈竖条，数字增加滚动，无数字不展示  数字右图左
---
实现过程：
1.Layout ， Image , Text 三个组件
2.Image 和 Text 分开实现没什么好说的 本来就是互不相干的两个东西 指责单一 不该放到一起
而且 Image 和 text 的位置不是固定的  写成一个view 太麻烦了
3.Layout 的作用是 用来实现点击缩放动画的， 为什么不在Image实现缩放动画呢？
假设 Image(宽40) 和 Text(宽40)  总计80的宽度 都能触发点击事件，如果动画在Image内实现
那么Text 和 Image之间 存在一个点击事件的绑定 不然点击text区域是无法触发动画的
所以判断 image ，text 被一个 Layout包裹 点击事件设置给 layout

 */
class JiKeActivity : AppCompatActivity() ,View.OnClickListener{
    private lateinit var mBinding: ActivityJiKeBinding
    private var checked = false
        set(value) {
            field = value
            mBinding.viewThumbsUp.checked = field
            mBinding.viewNumber.added = field
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityJiKeBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mBinding.viewThumbsUp.checked = checked
        mBinding.viewNumber.number = 218
        mBinding.tvReplyNumber.text = 90.toString()
        mBinding.tvForwardNumber.text = 32.toString()
        mBinding.containerThumbsUp.setOnClickListener(this)
        mBinding.containerReply.setOnClickListener(this)
        mBinding.containerForward.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view) {
            mBinding.containerReply -> {
                Toast.makeText(applicationContext,"回复了",Toast.LENGTH_SHORT).show()
            }
            mBinding.containerForward -> {
                Toast.makeText(applicationContext,"点赞了",Toast.LENGTH_SHORT).show()
            }
            mBinding.containerThumbsUp -> {
                checked = !checked
            }
        }
    }
}