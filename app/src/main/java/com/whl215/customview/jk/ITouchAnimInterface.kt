package com.whl215.customview.jk

/**
 * 定义 使View 触摸动画
 */
interface ITouchAnimInterface  {

    /**
     * @param isDown 触摸手势
     *               true MotionEvent.ACTION_DOWN
     */
    fun startAnim(isDown:Boolean)


}