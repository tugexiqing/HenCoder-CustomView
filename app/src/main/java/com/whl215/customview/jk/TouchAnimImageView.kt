package com.whl215.customview.jk

import android.content.Context
import android.util.AttributeSet
import android.view.animation.OvershootInterpolator
import androidx.appcompat.widget.AppCompatImageView

/**
 * 跳动缩放ImageView
 */
class TouchAnimImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : AppCompatImageView(context, attrs), ITouchAnimInterface {

    override fun startAnim(isDown: Boolean) {
        if (isDown) {
            animate()
                .scaleX(0.75f)
                .scaleY(0.75f)
                .start()
        } else {
            clearAnimation()
            scaleX = 0.75f
            scaleY = 0.75f
            animate()
                .scaleX(1f)
                .scaleY(1f)
                .setInterpolator(OvershootInterpolator())
                .start()

        }
    }


}